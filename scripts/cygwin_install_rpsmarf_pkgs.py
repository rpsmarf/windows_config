# Script to install a set of Cygwin packages from a file of package list names
# 
# Arguments:
# --32 for 32 bit cygwin installation
# --packages <pkgFileName>  - override default file name of cygwin_pkg_list.txt
from urllib import request
import sys, getopt, subprocess
build = 'x86_64'
packagefile = 'cygwin_pkg_list.txt'
opts, args = getopt.getopt(sys.argv[1:],'',['64','packages='])
for o,a in opts:
    if o == '--32': build = 'x86'
    if o == '--packages':
        packagefile = a
setupfile = "setup-%s.exe" % build
packages = ','.join([ x.strip() for x in open(packagefile).readlines()])
r = request.urlopen("http://cygwin.com/%s" % setupfile)
open(setupfile,'wb').write(r.read()) 
subprocess.call([setupfile, '-P', packages, '-q', '-l', "C:\\temp" ])
