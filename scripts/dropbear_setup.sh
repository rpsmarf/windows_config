#!/bin/bash

# Install sftp-server to the correct folder
CYGWIN_VER=`uname -m`
if [ "$CYGWIN_VER" == "x86_64" ] ;
then
   echo "Installing 64 bit version of sftp-server.exe"
   cp ../lib64/sftp-server.exe /usr/libexec/sftp-server.exe
else
   echo "Installing 32 bit version of sftp-server.exe"
   cp ../lib32/sftp-server.exe /usr/libexec/sftp-server.exe
fi
chmod a+x /usr/libexec/sftp-server.exe

cp rpsmarf_sshd_start.bat "/cygdrive/c/Users/$USER/AppData/Roaming/Microsoft/Windows/Start Menu/Programs/Startup"

cat ../keys/dropbear_key.pubssh >> ~/.ssh/authorized_keys

echo "source /cygdrive/c/git/windows_config/scripts/import_windows_env_var.sh" >> ~/.bashrc
