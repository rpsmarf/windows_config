#!/bin/bash

cp ../src/dropbear-2014.66.tar.gz ../build_output
cd ../build_output
gunzip dropbear-2014.66.tar.gz
tar xvf dropbear-2014.66.tar
cd dropbear-2014.66
./configure
make
