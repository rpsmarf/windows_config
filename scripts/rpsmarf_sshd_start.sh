#!/bin/bash

export PATH="/usr/local/bin:/usr/bin:/cygdrive/c/Windows/system32:/cygdrive/c/Windows:/cygdrive/c/Windows/System32/Wbem:/cygdrive/c/Python32"

CYGWIN_VER=`uname -m`
if [ "$CYGWIN_VER" == "x86_64" ] ;
then
   echo "Using 64 bit version of dropbear.exe"
   /cygdrive/c/git/windows_config/bin64/dropbear.exe -E -R -F
else
   echo "Using 32 bit version of dropbear.exe"
   /cygdrive/c/git/windows_config/bin32/dropbear.exe -E -R -F
fi    
echo Result is $#

