#!/bin/bash

cp ../src/openssh-6.7p1.tar.gz ../build_output
cd ../build_output
gunzip openssh-6.7p1.tar.gz
tar xvf openssh-6.7p1.tar
cd openssh-6.7p1
./configure
make
cp sftp-server.exe /usr/libexec
